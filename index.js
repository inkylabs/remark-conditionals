import { toText, splitContainer } from '@inkylabs/remark-utils'
import visit from 'unist-util-visit'

const NAME = 'conditionals'

function run (code, vars) {
  const declare = Object.keys(vars)
    .map(k => `let ${k} = vars['${k}']; `)
    .join('')
  // eslint-disable-next-line no-eval
  return eval(declare + code)
}

export default (opts) => {
  opts = Object.assign({
    data: {},
    vars: {}
  }, opts)

  return (root, f) => {
    const types = ['text-get', 'container-set', 'container-if']
    visit(root, types, (node, index, parent) => {
      if (node.type === 'container-set') {
        const [label, contents] = splitContainer(node)
        let items = contents
        if ('unwrap' in node.attributes) {
          if (items.length !== 1) {
            f.message('cannot use unwrap with multiple nodes', node.position,
              `${NAME}:unwrap-single`)
          } else {
            items = items[0].children
          }
        }

        opts.data[toText(label)] = items
        parent.children.splice(index, 1)
        return [visit.SKIP, index]
      }

      if (node.type === 'text-get') {
        const key = toText(node)
        if (!(key in opts.data)) {
          f.message(`no setter found for key "${key}"`, node.position,
            `${NAME}:key-defined`)
        }
        const n = opts.data[key] || []
        parent.children.splice(index, 1, ...n)
        return [visit.SKIP, index]
      }

      // node.type === 'container-if'
      const [label, contents] = splitContainer(node)
      f.messages = f.messages.filter(m =>
        m.source !== 'easytext' ||
        m.line !== label.position.start.line)
      const code = toText(node.labelNode)
      let repl
      try {
        repl = run(code, opts.vars) ? contents : []
      } catch (err) {
        f.message(`could not evaluate conditional statement "${code}": ${err}`,
          node.position, `${NAME}:clean-eval`)
        repl = []
      }
      parent.children.splice(index, 1, ...repl)
      return [visit.SKIP, index]
    })
  }
}
